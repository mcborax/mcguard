package net.bot.mcguard;

// @author ArtBorax
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.bot.mcguard.commands.LinkCmd;
import net.bot.mcguard.commands.ReportCmd;
import net.bot.mcguard.commands.ReportsCmd;
import net.bot.mcguard.commands.UnLinkCmd;
import net.bot.mcguard.listeners.PlayerEventHandler;
import net.bot.mcguard.map.*;
import net.bot.mcguard.map.ActionPlayer.Msgs;
import net.bot.mcguard.rabbitmq.IRabbitReceiving;
import net.bot.mcguard.rabbitmq.Msg;
import net.bot.mcguard.rabbitmq.MsgType;
import net.bot.mcguard.rabbitmq.Rabbit;
import net.minecraft.server.v1_7_R4.ChatClickable;
import net.minecraft.server.v1_7_R4.ChatHoverable;
import net.minecraft.server.v1_7_R4.ChatMessage;
import net.minecraft.server.v1_7_R4.ChatModifier;
import net.minecraft.server.v1_7_R4.EnumClickAction;
import net.minecraft.server.v1_7_R4.EnumHoverAction;
import net.minecraft.server.v1_7_R4.IChatBaseComponent;
import net.minecraft.server.v1_7_R4.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class MCGuard extends JavaPlugin {

    public String pluginName = "";
    private static MCGuard plugin;
    private Rabbit rabbit;
    private final HashMap<String, GuardPlayer> cachPlayer = new HashMap<>();
    private Properties properties;
    private FileConfiguration config = null;
    private File configFile = null;
    private int count = 0;

    private static final Pattern pCommands = Pattern.compile("\\(((.*?|\\s)*?)\\)\\[((.*?|\\s)*?)\\]");
    private static final Pattern pCommandsType = Pattern.compile("\\((...):((.*?|\\s)*?)\\)");

    public static MCGuard getInstance() {
        return plugin;
    }

    @Override
    public void onEnable() {
        plugin = this;
        pluginName = "[" + this.getDescription().getName() + "] ";
        saveDefaultConfig();
        rabbit = new Rabbit();
        getCommand("report").setExecutor(new ReportCmd(this));
        getCommand("reports").setExecutor(new ReportsCmd(this));
        getCommand("link").setExecutor(new LinkCmd(this));
        getCommand("unlink").setExecutor(new UnLinkCmd(this));
        getServer().getPluginManager().registerEvents(new PlayerEventHandler(this), this);
        scheduler();
        Bukkit.getLogger().info(pluginName + "Enable");
    }

    @Override
    public void onDisable() {
        sendRabbit("OFF", MsgType.SERVER_DOWN);
    }

    @Override
    public void reloadConfig() {
        if (configFile == null) {
            configFile = new File(getDataFolder(), "config.yml");
        }
        config = YamlConfiguration.loadConfiguration(configFile);
        InputStream defConfigStream = getResource("config.yml");
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            config.setDefaults(defConfig);
        }
    }

    @Override
    public FileConfiguration getConfig() {
        if (config == null) {
            reloadConfig();
        }
        return config;
    }

    @Override
    public void saveConfig() {
        if (config != null && configFile != null) {
            try {
                getConfig().save(configFile);
            } catch (IOException ex) {
                getServer().getConsoleSender().sendMessage(ChatColor.RED + pluginName + "Could not save config to " + configFile);
            }
        }
    }

    @Override
    public void saveDefaultConfig() {
        if (configFile == null) {
            configFile = new File(getDataFolder(), "config.yml");
        }
        if (!configFile.exists()) {
            saveResource("config.yml", false);
            getConfig().set("RabbitHost", "127.0.0.1");
            getConfig().set("RabbitPort", 5672);
            getConfig().set("RabbitUser", "user");
            getConfig().set("RabbitPass", "pass");
            getConfig().set("ExchangeNameC", "");
            getConfig().set("ExchangeNameS", "");
            getConfig().set("QueueNameC", "client");
            getConfig().set("QueueNameS", "server");
            getConfig().set("ServerID", 0);
            saveConfig();
            reloadConfig();
            getServer().getConsoleSender().sendMessage(pluginName + "Default config.yml saved.");
        }
    }

    public void countKarma(Player player, CountKarmaType type) {
        GuardPlayer gp = cachPlayer.get(player.getUniqueId().toString());
        if (gp == null) {
            return;
        }
        gp.countKarma(type);
    }

    public void removeCachPlayer(String uuid) {
        cachPlayer.remove(uuid);
    }

    private void scheduler() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            HashMap<String, GuardPlayer> gPlayers = (HashMap<String, GuardPlayer>) cachPlayer.clone();
            Set<Integer> players = new HashSet<>();
            for (GuardPlayer gp : gPlayers.values()) {
                if (gp.getPlayerId() != 0) {
                    players.add(gp.getPlayerId());
                    if (count == 0) {
                        if (gp.getActionPlayer() > 0) {
                            sendRabbit(new ActionPlayer(gp.getPlayerId(), gp.getActionPlayer(), MsgType.ACTION_PLAYER.toString()), MsgType.ACTION_PLAYER);
                            gp.resetCount();
                        }
                        Player player = Bukkit.getServer().getPlayer(UUID.fromString(gp.getUuid()));
                        if (player == null || !player.isOnline()) {
                            removeCachPlayer(gp.getUuid());
                        }
                    }
                }
            }
            sendRabbit(new ServerStat(getConfig().getInt("ServerID"), 20, players), MsgType.PING);
            count++;
            if (count >= 12) {
                count = 0;
            }
        }, 1L, 20 * 5);

    }

    public GuardPlayer getGuardPlayer(String uuid) {
        return cachPlayer.get(uuid);
    }

    public GuardPlayer getGuardPlayer(Player player) {
        return cachPlayer.get(player.getUniqueId().toString());
    }

    public void setGuardPlayer(String uuid, GuardPlayer gPlayer) {
        Player player = Bukkit.getServer().getPlayer(UUID.fromString(uuid));
        if (player != null && player.isOnline()) {
            cachPlayer.put(uuid, gPlayer);
        }
    }

    public void sendRabbit(Object msg, MsgType type) {
        try {
            rabbit.putMsg(new Msg(getConfig().getString("ExchangeNameC"), getConfig().getString("QueueNameC"), msg, type.getProps()));
        } catch (UnsupportedEncodingException ex) {
        }
    }

    public void sendMSGBroadcast(String msg) {
        Bukkit.getServer().broadcastMessage(msg);
    }

    public void sendMSGBroadcast(IChatBaseComponent msg) {
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(msg));
        }
    }

    public void sendMSG(String playerUUID, String msg) {
        if (playerUUID == null || playerUUID.isEmpty()) {
            Bukkit.getServer().broadcastMessage(msg);
        } else {
            Player player = Bukkit.getServer().getPlayer(UUID.fromString(playerUUID));
            if (player != null && player.isOnline()) {
                player.sendMessage(msg);
            }
        }
    }

    public void sendMSG(String playerUUID, IChatBaseComponent msg) {
        if (playerUUID == null || playerUUID.isEmpty()) {
            sendMSGBroadcast(msg);
        } else {
            Player player = Bukkit.getServer().getPlayer(UUID.fromString(playerUUID));
            if (player != null && player.isOnline()) {
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(msg));
            }
        }
    }

    public void sendMSGConfirm(String playerUUID, String msg, String cmd) {
        final Player player = Bukkit.getServer().getPlayer(UUID.fromString(playerUUID));
        if (player == null || !player.isOnline()) {
            return;
        }
//            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
//                @Override
//                public void run() {
        if (msg != null && !msg.isEmpty()) {
            player.chat(msg);
        } else if (cmd != null && !cmd.isEmpty()) {
            Bukkit.getServer().dispatchCommand(player, cmd);
        }
//                }
//            }, 1L);

    }

    public void rabbitListenerQueue() throws IOException {
        rabbit.queueDeclare(getConfig().getString("QueueNameS"), new IRabbitReceiving() {
            @Override
            public boolean receive(String messageId, String rawMsg) {
                switch (MsgType.getType(messageId)) {
                    case STAT_PLAYER:
                        PlayerStat playerStat = new Gson().fromJson(rawMsg, PlayerStat.class);
                        GuardPlayer gPlayer = getGuardPlayer(playerStat.getUuid());
                        if (gPlayer != null && gPlayer.getPlayerId() != 0) {
                            gPlayer.update(playerStat);
                        } else {
                            setGuardPlayer(playerStat.getUuid(), new GuardPlayer(playerStat));
                        }
                        break;
                    case KARMA_CHANGE:
                        playerStat = new Gson().fromJson(rawMsg, PlayerStat.class);
                        gPlayer = getGuardPlayer(playerStat.getUuid());
                        if (gPlayer != null && gPlayer.getPlayerId() != 0) {
                            if (gPlayer.getKarma() != 0 && ((int) gPlayer.getKarma()) != ((int) playerStat.getKarma())) {
                                ActionPlayer ap = new ActionPlayer();
                                ap.setPlayerId(gPlayer.getPlayerId());
                                ap.setType(MsgType.GET_SHOW_KARMA.toString());
                                plugin.sendRabbit(ap, MsgType.GET_SHOW_KARMA);
                            }
                            gPlayer.update(playerStat);
                        } else {
                            setGuardPlayer(playerStat.getUuid(), new GuardPlayer(playerStat));
                        }
                        break;
                    case CMD_MODERATION:
                        ActionPlayer ap = new Gson().fromJson(rawMsg, ActionPlayer.class);
                        gPlayer = getGuardPlayer(ap.getUuid());
                        if (gPlayer == null) {
                            gPlayer = new GuardPlayer(ap);
                        }
                        gPlayer.setConfirmation(ap.isConfirmation());
                        setGuardPlayer(ap.getUuid(), gPlayer);
                        break;
                    case CHAT_MSG_PLAYER:
                        ap = new Gson().fromJson(rawMsg, ActionPlayer.class);
                        printMsgOrCmd(ap.getUuid(), ap.getMsg(), ap.getCmd());
                        if (ap.getMsgs() != null) {
                            for (Msgs msg : ap.getMsgs()) {
                                printMsgOrCmd(ap.getUuid(), msg.getMsg(), msg.getCmd());
                            }
                        }
                        break;
                    case CHAT_MSG_BROADCAST:
                        ap = new Gson().fromJson(rawMsg, ActionPlayer.class);
                        if (ap.getMsg() != null && !ap.getMsg().isEmpty()) {
                            sendMSGBroadcast(ap.getMsg());
                        }
                        if (ap.getMsgs() != null) {
                            for (Msgs msg : ap.getMsgs()) {
                                if (msg.getMsg() != null && !msg.getMsg().isEmpty()) {
                                    sendMSGBroadcast(msg.getMsg());
                                }
                            }
                        }
                        break;
                    case CHAT_MSG_CONF:
                        ap = new Gson().fromJson(rawMsg, ActionPlayer.class);
                        sendMSGConfirm(ap.getUuid(), ap.getMsg(), ap.getCmd());
                        break;
                    case SYS_PROPERTIES:
                        properties = new Gson().fromJson(rawMsg, Properties.class);
                        break;
                    case REPORT_CREATE_ADD:
                    case REPORT_ADD:
                        ReportMessage rm = new Gson().fromJson(rawMsg, ReportMessage.class);
                        gPlayer = getGuardPlayer(rm.getPlayerUUID());
                        if (gPlayer != null && gPlayer.getPlayerId() != 0) {
                            gPlayer.setReport(rm);
                        }
                        break;
                    case UNKNOW:
                        return false;
                }
                return true;
            }
        });
    }

    private void printMsgOrCmd(String username, String msg, String cmd) {
        if (msg != null && !msg.isEmpty()) {
            if (cmd != null && !cmd.isEmpty()) {
                IChatBaseComponent builder = new ChatMessage("");
                Matcher m = pCommands.matcher(cmd);
                while (m.find()) {
                    if (m.group(1).isEmpty() || m.group(2).isEmpty()) {
                        continue;
                    }
                    StringBuffer buff = new StringBuffer();
                    m.appendReplacement(buff, "");
                    String text = buff.toString();
                    if (!text.isEmpty()) {
                        builder.addSibling(new ChatMessage(text));
                    }
                    IChatBaseComponent buildCmd = new ChatMessage(m.group(1));
                    buildCmd.setChatModifier(new ChatModifier());
                    Matcher mType = pCommandsType.matcher(m.group(3));
                    //cmd url sht hvr 
                    while (mType.find()) {
                        if (mType.group(1).equals("cmd")) {
                            buildCmd.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.RUN_COMMAND, "/".concat(mType.group(2))));
                        } else if (mType.group(1).equals("smd")) {
                            buildCmd.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.SUGGEST_COMMAND, "/".concat(mType.group(2))));
                        } else if (mType.group(1).equals("hvr")) {
                            buildCmd.getChatModifier().a(new ChatHoverable(EnumHoverAction.SHOW_TEXT, new ChatMessage(mType.group(2))));
                        } else if (mType.group(1).equals("sht")) {
                            //cmd.onShiftClick();
                        } else if (mType.group(1).equals("url")) {
                            buildCmd.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.OPEN_URL, "/".concat(mType.group(2))));
                        }
                    }
                    builder.addSibling(buildCmd);
                }
                String text = m.appendTail(new StringBuffer()).toString();
                if (!text.isEmpty()) {
                    builder.addSibling(new ChatMessage(text));
                }
                sendMSG(username, builder);
            } else {
                sendMSG(username, msg);
            }
        }
    }

    public boolean isModerationCmd(String cmd) {
        if (properties != null && properties.getModerationCmd() != null) {
            if (properties.getModerationCmd().contains(cmd.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

}
