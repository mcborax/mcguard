package net.bot.mcguard.rabbitmq;

// @author ArtBorax
public interface IRabbitReceiving {

    public boolean receive(String messageId, String msg);
}
