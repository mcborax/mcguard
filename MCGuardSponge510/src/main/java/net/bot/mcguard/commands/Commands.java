package net.bot.mcguard.commands;

// @author ArtBorax
import net.bot.mcguard.MCGuard;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

public class Commands {

    private final MCGuard plugin;

    public Commands(MCGuard plugin) {
        this.plugin = plugin;
        init();
    }

    private void init() {
        CommandSpec reportsCmd = CommandSpec.builder()
                .description(Text.of("Список репортов"))
                .permission("mcguard.command.reports")
                .arguments(GenericArguments.optional(GenericArguments.string(Text.of("player"))), GenericArguments.optional(GenericArguments.integer(Text.of("page"))))
                .executor(new ReportsExecutor(plugin))
                .build();

        CommandSpec logChatCmd = CommandSpec.builder()
                .description(Text.of("Лог чата"))
                .permission("mcguard.command.chatlog")
                .arguments(GenericArguments.optional(GenericArguments.string(Text.of("player"))), GenericArguments.optional(GenericArguments.integer(Text.of("page"))))
                .executor(new LogChatExecutor(plugin))
                .build();

        CommandSpec createReportCmd = CommandSpec.builder()
                .description(Text.of("Создать репорт"))
                .permission("mcguard.command.createreport")
                .arguments(GenericArguments.firstParsing(GenericArguments.user(Text.of("player")), GenericArguments.string(Text.of("user"))), GenericArguments.optional(GenericArguments.integer(Text.of("type"))), GenericArguments.optional(GenericArguments.integer(Text.of("level"))), GenericArguments.optional(GenericArguments.remainingJoinedStrings(Text.of("text"))))
                .executor(new CreateReportExecutor(plugin))
                .build();

        CommandSpec addReportCmd = CommandSpec.builder()
                .description(Text.of("Дополнить репорт"))
                .permission("mcguard.command.addreport")
                .arguments(GenericArguments.integer(Text.of("number")))
                .executor(new AddReportExecutor(plugin))
                .build();

        CommandSpec infoReportCmd = CommandSpec.builder()
                .description(Text.of("Информация о репорте"))
                .permission("mcguard.command.info")
                .arguments(GenericArguments.integer(Text.of("number")))
                .executor(new InfoReportExecutor(plugin))
                .build();

        CommandSpec statPlayerCmd = CommandSpec.builder()
                .description(Text.of("Статистика игрока"))
                .permission("mcguard.command.info")
                .arguments(GenericArguments.optional(GenericArguments.firstParsing(GenericArguments.user(Text.of("player")), GenericArguments.string(Text.of("user")))))
                .executor(new StatExecutor(plugin))
                .build();

        CommandSpec consReportCmd = CommandSpec.builder()
                .description(Text.of("Рассмотреть репорт"))
                .permission("mcguard.command.consider")
                .arguments(GenericArguments.integer(Text.of("number")), GenericArguments.optional(GenericArguments.integer(Text.of("level"))), GenericArguments.optional(GenericArguments.remainingJoinedStrings(Text.of("text"))))
                .executor(new ConsiderReportExecutor(plugin))
                .build();

        CommandSpec сompleteReportCmd = CommandSpec.builder()
                .description(Text.of("Закончить дополнение"))
                .permission("mcguard.command.addreport")
                .executor(new CompleteReportExecutor(plugin))
                .build();

        CommandSpec cancelReportCmd = CommandSpec.builder()
                .description(Text.of("Отменить дополнение"))
                .permission("mcguard.command.addreport")
                .executor(new CancelReportExecutor(plugin))
                .build();

        CommandSpec topCmd = CommandSpec.builder()
                .description(Text.of("Топ игроков с высокой кармой"))
                .permission("mcguard.command.info")
                .executor(new TopExecutor(plugin))
                .build();

        CommandSpec linkCmd = CommandSpec.builder()
                .description(Text.of("Связать аккаунт с дискордом"))
                .permission("mcguard.command.info")
                .arguments(GenericArguments.optional(GenericArguments.integer(Text.of("kode"))))
                .executor(new LinkExecutor(plugin))
                .build();

        CommandSpec unLinkCmd = CommandSpec.builder()
                .description(Text.of("Отвязать аккаунт от дискорда!"))
                .permission("mcguard.command.info")
                .executor(new UnLinkExecutor(plugin))
                .build();

        CommandSpec cmd = CommandSpec.builder()
                .description(Text.of("Меню системы репортов"))
                .permission("mcguard.command.main")
                .executor(new MenuExecutor(plugin))
                .child(logChatCmd, "log")
                .child(createReportCmd, "new")
                .child(addReportCmd, "add")
                .child(сompleteReportCmd, "complete")
                .child(cancelReportCmd, "cancel")
                .child(consReportCmd, "consider")
                .child(infoReportCmd, "info")
                .child(statPlayerCmd, "stat")
                .child(topCmd, "top")
                .build();

        Sponge.getCommandManager().register(plugin, reportsCmd, "reports");
        Sponge.getCommandManager().register(plugin, cmd, "report");
        Sponge.getCommandManager().register(plugin, linkCmd, "link");
        Sponge.getCommandManager().register(plugin, unLinkCmd, "unlink");
    }
}
