package net.bot.mcguard.commands;

// @author ArtBorax
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.ActionPlayer;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.rabbitmq.MsgType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;

public class StatExecutor implements CommandExecutor {

    private final MCGuard plugin;

    public StatExecutor(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("Только из игры."));
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0) {
            throw new CommandException(Text.of("Команда временно недоступна, попробуйте повторить попытку через 5 секунд."));
        }
        ActionPlayer ap = new ActionPlayer();
        ap.setPlayerId(gp.getPlayerId());
        ap.setType(MsgType.GET_PLAYER_STAT.toString());
        if (ctx.<User>getOne("player").isPresent()) {
            User user = ctx.<User>getOne("player").get();
            ap.setUsername(user.getName());
            ap.setUuid(user.getUniqueId().toString());
        } else if (ctx.<String>getOne("user").isPresent()) {
            ap.setUsername(ctx.<String>getOne("user").get());
        } else {
            ap.setUsername(player.getName());
        }
        plugin.sendRabbit(ap, MsgType.GET_PLAYER_STAT);
        return CommandResult.success();
    }

}
