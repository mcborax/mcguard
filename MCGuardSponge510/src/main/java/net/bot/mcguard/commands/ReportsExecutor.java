package net.bot.mcguard.commands;

// @author ArtBorax
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.ActionPlayer;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.rabbitmq.MsgType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class ReportsExecutor implements CommandExecutor {

    private final MCGuard plugin;

    public ReportsExecutor(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("Только из игры."));
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0) {
            throw new CommandException(Text.of("Команда временно недоступна, попробуйте повторить попытку через 5 секунд."));
        }
        if (gp.isReport()) {
            throw new CommandException(Text.of("Ты уже добавляешь описание для репорта: " + gp.getReport().getReportId() + "! Закончи этот процесс сначала!!!"));
        }
        ActionPlayer ap = new ActionPlayer();
        ap.setPlayerId(gp.getPlayerId());
        ap.setType(MsgType.GET_REPORTS.toString());
        if (ctx.<String>getOne("player").isPresent()) {
            String user = ctx.<String>getOne("player").get();
            try {
                ap.setMsg(Integer.toString(Integer.parseInt(user)));
            } catch (Exception ex) {
                ap.setUsername(user);
            }
        }
        if (ctx.<Integer>getOne("page").isPresent()) {
            ap.setMsg(Integer.toString(ctx.<Integer>getOne("page").get()));
        }
        plugin.sendRabbit(ap, MsgType.GET_REPORTS);
        return CommandResult.success();
    }

}
