package net.bot.mcguard;

// @author ArtBorax
import com.google.gson.Gson;
import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.bot.mcguard.commands.Commands;
import net.bot.mcguard.listeners.PlayerEventHandler;
import net.bot.mcguard.map.*;
import net.bot.mcguard.map.ActionPlayer.Msgs;
import net.bot.mcguard.rabbitmq.Msg;
import net.bot.mcguard.rabbitmq.MsgType;
import net.bot.mcguard.rabbitmq.Rabbit;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.NamedCause;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.LiteralText.Builder;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.channel.MessageChannel;

@Plugin(id = "mcguard", name = "MCGuard", version = "1.0", description = "Guardian Borax")
public class MCGuard {

    private static MCGuard plugin;
    private File rootDir;
    private Rabbit rabbit;
    private final HashMap<String, GuardPlayer> cachPlayer = new HashMap<>();
    private Properties properties;
    private int count = 0;

    private static final Pattern pCommands = Pattern.compile("\\(((.*?|\\s)*?)\\)\\[((.*?|\\s)*?)\\]");
    private static final Pattern pCommandsType = Pattern.compile("\\((...):((.*?|\\s)*?)\\)");

    @Inject
    private Logger logger;

    public static MCGuard getInstance() {
        return plugin;
    }

    public Logger getLogger() {
        return logger;
    }

    public File getRootDir() {
        return rootDir;
    }

    @Inject
    @ConfigDir(sharedRoot = true)
    private File defaultConfigDir;

    @Listener
    public void onServerInit(GameInitializationEvent event) {
        getLogger().info("§eLoading...");
        plugin = this;
        rootDir = new File(defaultConfigDir, "MCGuard");
        Config.init(rootDir);
    }

    @Listener
    public void onStart(GameStartedServerEvent event) {
        Config.load();
        rabbit = new Rabbit();
        new Commands(plugin);
        Sponge.getEventManager().registerListeners(this, new PlayerEventHandler(this));
        scheduler();
        getLogger().info("§aLoaded!");
    }

    @Listener
    public void onServerStop(GameStoppedServerEvent event) {
        sendRabbit("OFF", MsgType.SERVER_DOWN);
    }

    public void countKarma(Player player, CountKarmaType type) {
        GuardPlayer gp = cachPlayer.get(player.getUniqueId().toString());
        if (gp == null) {
            return;
        }
        gp.countKarma(type);
    }

    public void removeCachPlayer(String uuid) {
        cachPlayer.remove(uuid);
    }

    private void scheduler() {
        Sponge.getScheduler().createTaskBuilder().intervalTicks(20 * 5).execute(() -> {
            HashMap<String, GuardPlayer> gPlayers = (HashMap<String, GuardPlayer>) cachPlayer.clone();
            Set<Integer> players = new HashSet<>();
            for (GuardPlayer gp : gPlayers.values()) {
                if (gp.getPlayerId() != 0) {
                    players.add(gp.getPlayerId());
                    if (count == 0) {
                        if (gp.getActionPlayer() > 0) {
                            sendRabbit(new ActionPlayer(gp.getPlayerId(), gp.getActionPlayer(), MsgType.ACTION_PLAYER.toString()), MsgType.ACTION_PLAYER);
                            gp.resetCount();
                        }
                        if (!Sponge.getServer().getPlayer(UUID.fromString(gp.getUuid())).isPresent()) {
                            removeCachPlayer(gp.getUuid());
                        }
                    }
                }
            }
            sendRabbit(new ServerStat(Config.getNode("ServerID").getInt(), 20, players), MsgType.PING);
            count++;
            if (count >= 12) {
                count = 0;
            }
        }).submit(plugin);
    }

    public GuardPlayer getGuardPlayer(String uuid) {
        return cachPlayer.get(uuid);
    }

    public GuardPlayer getGuardPlayer(Player player) {
        return cachPlayer.get(player.getUniqueId().toString());
    }

    public void setGuardPlayer(String uuid, GuardPlayer gPlayer) {
        if (Sponge.getServer().getPlayer(UUID.fromString(uuid)).isPresent()) {
            cachPlayer.put(uuid, gPlayer);
        }
    }

    public void sendRabbit(Object msg, MsgType type) {
        try {
            rabbit.putMsg(new Msg(Config.getNode("ExchangeNameC").getString(), Config.getNode("QueueNameC").getString(), msg, type.getProps()));
        } catch (UnsupportedEncodingException ex) {
        }
    }

    public void sendMSGBroadcast(String msg) {
        MessageChannel.TO_ALL.send(Text.of(msg));
    }

    public void sendMSG(String playerUUID, String msg) {
        if (playerUUID == null || playerUUID.isEmpty()) {
            MessageChannel.TO_ALL.send(Text.of(msg));
        } else {
            sendMSG(UUID.fromString(playerUUID), Text.of(msg));
        }
    }

    public void sendMSG(String playerUUID, Text msg) {
        if (playerUUID == null || playerUUID.isEmpty()) {
            MessageChannel.TO_ALL.send(msg);
        } else {
            sendMSG(UUID.fromString(playerUUID), msg);
        }
    }

    public void sendMSG(UUID playerUUID, Text msg) {
        Sponge.getServer().getPlayer(playerUUID).ifPresent(p -> p.sendMessage(msg));
    }

    public void sendMSGConfirm(String playerUUID, String msg, String cmd) {
        Sponge.getServer().getPlayer(UUID.fromString(playerUUID)).ifPresent((Player player) -> {
            Sponge.getScheduler().createTaskBuilder().delayTicks(1).execute(() -> {
                GuardPlayer gPlayer = plugin.getGuardPlayer(player);
                if (gPlayer == null) {
                    return;
                }
                if (msg != null && !msg.isEmpty()) {
                    player.simulateChat(Text.of(msg), Cause.of(NamedCause.simulated(player)));
                } else if (cmd != null && !cmd.isEmpty()) {
                    System.out.println("res: ["+cmd+"]");
                    gPlayer.addConfMsg(cmd);
                    Sponge.getCommandManager().process(player, cmd);
                }
            }).submit(plugin);
        });

    }

    public void rabbitListenerQueue() throws IOException {
        rabbit.queueDeclare(Config.getNode("QueueNameS").getString(), (String messageId, String rawMsg) -> {
            switch (MsgType.getType(messageId)) {
                case STAT_PLAYER:
                    PlayerStat playerStat = new Gson().fromJson(rawMsg, PlayerStat.class);
                    GuardPlayer gPlayer = getGuardPlayer(playerStat.getUuid());
                    if (gPlayer != null && gPlayer.getPlayerId() != 0) {
                        gPlayer.update(playerStat);
                    } else {
                        setGuardPlayer(playerStat.getUuid(), new GuardPlayer(playerStat));
                    }
                    break;
                case KARMA_CHANGE:
                    playerStat = new Gson().fromJson(rawMsg, PlayerStat.class);
                    gPlayer = getGuardPlayer(playerStat.getUuid());
                    if (gPlayer != null && gPlayer.getPlayerId() != 0) {
                        if (gPlayer.getKarma() != 0 && ((int) gPlayer.getKarma()) != ((int) playerStat.getKarma())) {
                            ActionPlayer ap = new ActionPlayer();
                            ap.setPlayerId(gPlayer.getPlayerId());
                            ap.setType(MsgType.GET_SHOW_KARMA.toString());
                            plugin.sendRabbit(ap, MsgType.GET_SHOW_KARMA);
                        }
                        gPlayer.update(playerStat);
                    } else {
                        setGuardPlayer(playerStat.getUuid(), new GuardPlayer(playerStat));
                    }
                    break;
                case CMD_MODERATION:
                    ActionPlayer ap = new Gson().fromJson(rawMsg, ActionPlayer.class);
                    gPlayer = getGuardPlayer(ap.getUuid());
                    if (gPlayer == null) {
                        gPlayer = new GuardPlayer(ap);
                    }
                    gPlayer.setConfirmation(ap.isConfirmation());
                    setGuardPlayer(ap.getUuid(), gPlayer);
                    break;
                case CHAT_MSG_PLAYER:
                    ap = new Gson().fromJson(rawMsg, ActionPlayer.class);
                    printMsgOrCmd(ap.getUuid(), ap.getMsg(), ap.getCmd());
                    if (ap.getMsgs() != null) {
                        for (Msgs msg : ap.getMsgs()) {
                            printMsgOrCmd(ap.getUuid(), msg.getMsg(), msg.getCmd());
                        }
                    }
                    break;
                case CHAT_MSG_BROADCAST:
                    ap = new Gson().fromJson(rawMsg, ActionPlayer.class);
                    if (ap.getMsg() != null && !ap.getMsg().isEmpty()) {
                        sendMSGBroadcast(ap.getMsg());
                    }
                    if (ap.getMsgs() != null) {
                        for (Msgs msg : ap.getMsgs()) {
                            if (msg.getMsg() != null && !msg.getMsg().isEmpty()) {
                                sendMSGBroadcast(msg.getMsg());
                            }
                        }
                    }
                    break;
                case CHAT_MSG_CONF:
                    ap = new Gson().fromJson(rawMsg, ActionPlayer.class);
                    sendMSGConfirm(ap.getUuid(), ap.getMsg(), ap.getCmd());
                    break;
                case SYS_PROPERTIES:
                    properties = new Gson().fromJson(rawMsg, Properties.class);
                    break;
                case REPORT_CREATE_ADD:
                case REPORT_ADD:
                    ReportMessage rm = new Gson().fromJson(rawMsg, ReportMessage.class);
                    gPlayer = getGuardPlayer(rm.getPlayerUUID());
                    if (gPlayer != null && gPlayer.getPlayerId() != 0) {
                        gPlayer.setReport(rm);
                    }
                    break;
                case UNKNOW:
                    return false;
            }
            return true;
        });
    }

    private void printMsgOrCmd(String uuid, String msg, String cmd) {
        if (msg != null && !msg.isEmpty()) {
            if (cmd != null && !cmd.isEmpty()) {

                Builder builder = Text.builder("");
                Matcher m = pCommands.matcher(cmd);
                while (m.find()) {
                    if (m.group(1).isEmpty() || m.group(2).isEmpty()) {
                        continue;
                    }
                    StringBuffer buff = new StringBuffer();
                    m.appendReplacement(buff, "");
                    String text = buff.toString();
                    if (!text.isEmpty()) {
                        builder.append(Text.of(text));
                    }
                    Builder buildCmd = Text.builder(m.group(1));
                    Matcher mType = pCommandsType.matcher(m.group(3));
                    //cmd url sht hvr 
                    while (mType.find()) {
                        if (mType.group(1).equals("cmd")) {
                            buildCmd.onClick(TextActions.runCommand("/" + mType.group(2)));
                        } else if (mType.group(1).equals("smd")) {
                            buildCmd.onClick(TextActions.suggestCommand("/" + mType.group(2)));
                        } else if (mType.group(1).equals("hvr")) {
                            buildCmd.onHover(TextActions.showText(Text.of(mType.group(2))));
                        } else if (mType.group(1).equals("sht")) {
                            //cmd.onShiftClick();
                        } else if (mType.group(1).equals("url")) {
                            try {
                                buildCmd.onClick(TextActions.openUrl(new URL(mType.group(2))));
                            } catch (MalformedURLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    builder.append(buildCmd.build());
                }
                String text = m.appendTail(new StringBuffer()).toString();
                if (!text.isEmpty()) {
                    builder.append(Text.of(text));
                }
                sendMSG(uuid, builder.build());
            } else {
                sendMSG(uuid, msg);
            }
        }
    }

//    public static Optional<Player> checkSimulated(Event event) {
//        if (event.getContext().containsKey(EventContextKeys.PLAYER_SIMULATED)) {
//            GameProfile gp = event.getContext().get(EventContextKeys.PLAYER_SIMULATED).get();
//            return Sponge.getServer().getPlayer(gp.getUniqueId());
//        }
//        return Optional.empty();
//    }
    public Cause getCause() {
        return Cause.source(Sponge.getPluginManager().fromInstance(this).get()).build();
    }

    public boolean isModerationCmd(String cmd) {
        if (properties != null && properties.getModerationCmd() != null) {
            if (properties.getModerationCmd().contains(cmd.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

}
