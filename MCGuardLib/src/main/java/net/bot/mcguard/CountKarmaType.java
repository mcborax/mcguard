package net.bot.mcguard;

// @author ArtBorax
public enum CountKarmaType {
    CHAT,
    CMD,
    INTERACT,
    BLOCK,
    DEATH,
    DROP,
    DROPDESTR,
    INTERACTENTITY,
    CHANGEINV,
    RESET,
    UNKNOWN;
}
