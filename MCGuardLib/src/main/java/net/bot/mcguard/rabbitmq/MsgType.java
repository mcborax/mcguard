package net.bot.mcguard.rabbitmq;

// @author ArtBorax
import com.rabbitmq.client.AMQP.BasicProperties;
import java.util.HashMap;

public enum MsgType {

    CHAT_MSG("chat_msg", 86400),
    CHAT_MSG_BROADCAST("chat_msg_broadcast", 60),
    CHAT_MSG_PLAYER("chat_msg_player", 60),
    CHAT_MSG_REPORT("chat_msg_report", 60),
    CHAT_MSG_CONF("chat_msg_conf", 60),
    STAT_PLAYER("stat_player", 60),
    KARMA_CHANGE("karma_change", 60),
    CMD_MODERATION("cmd_moderation", 60),
    SYS_PROPERTIES("sys_properties", 60),
    ACTION_PLAYER("action_player", 60),
    PING("ping", 5),
    PLAYER_JOIN("player_join", 60),
    PLAYER_QUIT("player_quit", 60),
    GET_REPORTS("get_reports", 5),
    GET_LOG_CHAT("get_log_chat", 5),
    GET_MENU_MAIN("get_menu_main", 5),
    GET_SHOW_KARMA("get_show_karma", 5),
    GET_TOP_KARMA("get_top_karma", 5),
    GET_PLAYER_STAT("get_player_stat", 5),
    REPORT_CREATE("report_create", 5),
    REPORT_CREATE_COMPL("report_create_compl", 5),
    REPORT_CREATE_ADD("report_create_add", 5),
    REPORT_ADD("report_add", 5),
    REPORT_ADD_COMPL("report_add_compl", 5),
    REPORT_INFO("report_info", 5),
    REPORT_CONS("report_cons", 5),
    DISCORD_LINK("discord_link", 5),
    SERVER_DOWN("server_down", 5),
    UNKNOW("unknow", 1);

    private final HashMap<String, BasicProperties> props = new HashMap<>();
    private final int expiration;
    private final String msgId;

    MsgType(String msgId, int expiration) {
        this.msgId = msgId;
        this.expiration = expiration;
    }

    public void setServerId(int serverId) {
        props.put(msgId, new BasicProperties(null, null, null, 2, null, null, null, Integer.toString(expiration * 1000), msgId, null, null, null, Integer.toString(serverId), null));
    }

    public BasicProperties getProps() {
        return props.get(msgId);
    }

    @Override
    public String toString() {
        return msgId;
    }

    public static MsgType getType(String type) {
        if (type == null || type.isEmpty()) {
            return UNKNOW;
        }
        for (MsgType etype : MsgType.values()) {
            if (type.equals(etype.msgId)) {
                return etype;
            }
        }
        return UNKNOW;
    }

}
