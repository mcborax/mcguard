package net.bot.mcguard.map;

// @author ArtBorax
import java.util.Set;

public class ServerStat {

    private int serverId;
    private double tps;
    private Set<Integer> players;

    public ServerStat(int serverId, double tps, Set<Integer> players) {
        this.serverId = serverId;
        this.tps = tps;
        this.players = players;
    }

}
