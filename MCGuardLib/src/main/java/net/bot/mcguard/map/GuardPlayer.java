package net.bot.mcguard.map;

// @author ArtBorax
import java.util.ArrayList;
import net.bot.mcguard.CountKarmaType;

public class GuardPlayer {

    private int playerId = 0;
    private String username;
    private String uuid;
    private double karma;
    private double karmaSpeed;
    private boolean confirmation;
    private ReportMessage report;
    private ArrayList<String> confMsgs;

    private int countChat = 0;
    private int countCmd = 0;
    private int countBlock = 0;
    private int countChangeInv = 0;
    private int countDeath = 0;
    private int countDrop = 0;
    private int countDropDestr = 0;
    private int countInteract = 0;
    private int countInteractEntity = 0;

    public GuardPlayer(PlayerStat playerStat) {
        playerId = playerStat.getPlayerId();
        username = playerStat.getUsername();
        uuid = playerStat.getUuid();
        karma = playerStat.getKarma();
        karmaSpeed = playerStat.getKarmaSpeed();
    }

    public GuardPlayer(ActionPlayer chat) {
        playerId = chat.getPlayerId();
        username = chat.getUsername();
        uuid = chat.getUuid();
    }

    public void update(PlayerStat playerStat) {
        karma = playerStat.getKarma();
        karmaSpeed = playerStat.getKarmaSpeed();
    }

    public void countKarma(CountKarmaType type) {
        switch (type) {
            case BLOCK:
                countBlock = 1;
                break;
            case CHANGEINV:
                countChangeInv = 1;
                break;
            case CHAT:
                countChat = 1;
                break;
            case CMD:
                countCmd = 1;
                break;
            case DEATH:
                countDeath = 1;
                break;
            case DROP:
                countDrop = 1;
                break;
            case DROPDESTR:
                countDropDestr = 1;
                break;
            case INTERACT:
                countInteract = 1;
                break;
            case INTERACTENTITY:
                countInteractEntity = 1;
                break;
            case RESET:
                resetCount();
                break;
            case UNKNOWN:
                break;
        }
    }

    public void resetCount() {
        countBlock = 0;
        countChangeInv = 0;
        countChat = 0;
        countCmd = 0;
        countDeath = 0;
        countDrop = 0;
        countDropDestr = 0;
        countInteract = 0;
        countInteractEntity = 0;
    }

    public int getActionPlayer() {
        int result = countBlock + countChangeInv + countChat + countCmd + countDeath + countDrop + countDropDestr + countInteract + countInteractEntity;
        return result;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public double getKarma() {
        return karma;
    }

    public double getKarmaSpeed() {
        return karmaSpeed;
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    public boolean isReport() {
        return report != null;
    }

    public void setReport(ReportMessage report) {
        this.report = report;
    }

    public ReportMessage getReport() {
        return report;
    }

    public void addConfMsg(String msg) {
        if (confMsgs == null) {
            confMsgs = new ArrayList<>();
        }
        confMsgs.add(msg);
    }

    public boolean isConfMsg(String msg) {
        if (confMsgs == null) {
            return false;
        }
        return confMsgs.remove(msg);
    }
}
