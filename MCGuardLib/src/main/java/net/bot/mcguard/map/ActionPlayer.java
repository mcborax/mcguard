package net.bot.mcguard.map;

// @author ArtBorax
import java.util.Date;
import java.util.List;

public class ActionPlayer {

    private int playerId;
    private String username;
    private String uuid;
    private String msg;
    private int serverId;
    private Date datetime;
    private boolean confirmation;
    private String cmd;
    private int karma;
    private String type;
    private List<Msgs> msgs;

    public class Msgs {

        private String msg;
        private String cmd;

        public Msgs(String msg, String cmd) {
            this.msg = msg;
            this.cmd = cmd;
        }

        public String getMsg() {
            return msg;
        }

        public String getCmd() {
            return cmd;
        }

    }

    public ActionPlayer(String username, String uuid, String msg, String cmd, Date datetime, String type) {
        this.playerId = 0;
        this.username = username;
        this.uuid = uuid;
        this.msg = msg;
        this.cmd = cmd;
        this.datetime = datetime;
        this.type = type;
    }

    public ActionPlayer(GuardPlayer gPlayer, String msg, String cmd, Date datetime, String type) {
        this.playerId = gPlayer.getPlayerId();
        this.username = gPlayer.getUsername();
        this.uuid = gPlayer.getUuid();
        this.confirmation = gPlayer.isConfirmation();
        this.msg = msg;
        this.cmd = cmd;
        this.datetime = datetime;
        this.type = type;
    }

    public ActionPlayer(GuardPlayer gPlayer, boolean confirmation, String msg, String cmd, Date datetime, String type) {
        this.playerId = gPlayer.getPlayerId();
        this.username = gPlayer.getUsername();
        this.uuid = gPlayer.getUuid();
        this.confirmation = confirmation;
        this.msg = msg;
        this.cmd = cmd;
        this.datetime = datetime;
        this.type = type;
    }

    public ActionPlayer(int playerId, int level, String type) {
        this.playerId = playerId;
        this.karma = level;
        this.type = type;
    }

    public ActionPlayer(int playerId, String type, Date datetime) {
        this.playerId = playerId;
        this.type = type;
        this.datetime = datetime;
    }

    public ActionPlayer(String username, String uuid, String type, Date datetime) {
        this.playerId = 0;
        this.username = username;
        this.uuid = uuid;
        this.type = type;
        this.datetime = datetime;
    }

    public ActionPlayer() {
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getKarma() {
        return karma;
    }

    public void setKarma(int karma) {
        this.karma = karma;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Msgs> getMsgs() {
        return msgs;
    }
}
