package net.bot.mcguard.map;

// @author ArtBorax
import java.util.ArrayList;
import java.util.Date;

public class ReportMessage {

    private int serverId;
    private int playerId;
    private int reportId;
    private String playerUUID;
    private String username;    
    private String accusedUsername;
    private String accusedUUID;
    private String msg;
    private ArrayList<String> msgs;
    private Integer type;
    private int level;
    private Date time;
    private String typeMsg;

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    public void setAccusedUsername(String accusedUsername) {
        this.accusedUsername = accusedUsername;
    }

    public void setAccusedUUID(String accusedUUID) {
        this.accusedUUID = accusedUUID;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setMsgs(ArrayList<String> msgs) {
        this.msgs = msgs;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setTypeMsg(String typeMsg) {
        this.typeMsg = typeMsg;
    }

    public String getTypeMsg() {
        return typeMsg;
    }

    public int getPlayerId() {
        return playerId;
    }

    public String getPlayerUUID() {
        return playerUUID;
    }

    public ArrayList<String> getMsgs() {
        return msgs;
    }

    public int getReportId() {
        return reportId;
    }

    public void addMsg(String msg) {
        if (msgs == null) {
            msgs = new ArrayList<>();
        }
        msgs.add(msg);
    }

    public String getUsername() {
        return username;
    }


}
