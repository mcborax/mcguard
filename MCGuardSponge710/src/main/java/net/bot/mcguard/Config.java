package net.bot.mcguard;

import java.io.File;
import java.io.IOException;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class Config {

    public static File configFile;
    public static ConfigurationLoader<CommentedConfigurationNode> configManager;
    public static CommentedConfigurationNode config;

    public static void init(File rootDir) {
        configFile = new File(rootDir, "mcguard.conf");
        configManager = HoconConfigurationLoader.builder().setPath(configFile.toPath()).build();
    }

    public static void load() {
        load(null);
    }

    public static void load(CommandSource src) {
        // load file
        try {
            if (!configFile.exists()) {
                configFile.getParentFile().mkdirs();
                configFile.createNewFile();
                config = configManager.load();
                configManager.save(config);
            }
            config = configManager.load();
        } catch (IOException e) {
            MCGuard.getInstance().getLogger().error("Error load config");
            e.printStackTrace();
            if (src != null) {
                src.sendMessage(Text.of(TextColors.RED, "Error!!!"));
            }
            return;
        }

        Utils.ensureString(config.getNode("RabbitHost"), "127.0.0.1");
        Utils.ensurePositiveNumber(config.getNode("RabbitPort"), 5672);
        Utils.ensureString(config.getNode("RabbitUser"), "user");
        Utils.ensureString(config.getNode("RabbitPass"), "pass");
        Utils.ensureString(config.getNode("ExchangeNameC"), "");
        Utils.ensureString(config.getNode("ExchangeNameS"), "");
        Utils.ensureString(config.getNode("QueueNameC"), "client");
        Utils.ensureString(config.getNode("QueueNameS"), "server");
        Utils.ensurePositiveNumber(config.getNode("ServerID"), 0);

        save();
        if (src != null) {
            src.sendMessage(Text.of(TextColors.GREEN, "Saved"));
        }
    }

    public static void save() {
        try {
            configManager.save(config);
        } catch (IOException e) {
            MCGuard.getInstance().getLogger().error("Could not save config file !");
        }
    }

    public static CommentedConfigurationNode getNode(String... path) {
        return config.getNode((Object[]) path);
    }

    public static class Utils {

        public static void ensureString(CommentedConfigurationNode node, String def) {
            if (node.getString() == null) {
                node.setValue(def);
            }
        }

        public static void ensurePositiveNumber(CommentedConfigurationNode node, Number def) {
            if (!(node.getValue() instanceof Number) || node.getDouble(-1) < 0) {
                node.setValue(def);
            }
        }

        public static void ensureBoolean(CommentedConfigurationNode node, boolean def) {
            if (!(node.getValue() instanceof Boolean)) {
                node.setValue(def);
            }
        }
    }

}
