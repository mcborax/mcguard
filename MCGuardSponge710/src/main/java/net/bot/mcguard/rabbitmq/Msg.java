package net.bot.mcguard.rabbitmq;

// @author ArtBorax
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP.BasicProperties;
import java.io.UnsupportedEncodingException;

public class Msg {

    private final byte[] msg;
    private final String queueName;
    private final String exchangeName;
    private final BasicProperties props;

    public Msg(String exchangeName, String queueName, Object objMsg, BasicProperties props) throws UnsupportedEncodingException {
        this.exchangeName = exchangeName;
        this.queueName = queueName;
        this.msg = new Gson().toJson(objMsg).getBytes("UTF-8");
        this.props = props;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public String getQueueName() {
        return queueName;
    }

    public byte[] getMsg() {
        return msg;
    }

    public BasicProperties getProps() {
        return props;
    }

}
