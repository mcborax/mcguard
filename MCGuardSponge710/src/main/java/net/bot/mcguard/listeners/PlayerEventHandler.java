package net.bot.mcguard.listeners;

// @author ArtBorax
import java.time.Instant;
import java.util.Date;
import net.bot.mcguard.CountKarmaType;
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.ActionPlayer;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.rabbitmq.MsgType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.action.InteractEvent;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.filter.IsCancelled;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.item.inventory.ChangeInventoryEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.util.Tristate;

public class PlayerEventHandler {

    private final MCGuard plugin;

    public PlayerEventHandler(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Listener
    public void onPlayerJoin(ClientConnectionEvent.Join event) {
        Player player = event.getTargetEntity();
        plugin.countKarma(player, CountKarmaType.RESET);
        GuardPlayer gPlayer = plugin.getGuardPlayer(player);
        if (gPlayer != null && gPlayer.getPlayerId() != 0) {
            plugin.sendRabbit(new ActionPlayer(gPlayer.getPlayerId(), MsgType.PLAYER_JOIN.toString(), Date.from(Instant.now())), MsgType.PLAYER_JOIN);
        } else {
            plugin.sendRabbit(new ActionPlayer(player.getName(), player.getUniqueId().toString(), MsgType.PLAYER_JOIN.toString(), Date.from(Instant.now())), MsgType.PLAYER_JOIN);
        }
    }

    @Listener
    public void onPlayerQuit(ClientConnectionEvent.Disconnect event) {
        Player player = event.getTargetEntity();
        GuardPlayer gPlayer = plugin.getGuardPlayer(player);
        if (gPlayer != null && gPlayer.getPlayerId() != 0) {
            plugin.sendRabbit(new ActionPlayer(gPlayer.getPlayerId(), MsgType.PLAYER_QUIT.toString(), Date.from(Instant.now())), MsgType.PLAYER_QUIT);
            plugin.removeCachPlayer(player.getUniqueId().toString());
        } else {
            plugin.sendRabbit(new ActionPlayer(player.getName(), player.getUniqueId().toString(), MsgType.PLAYER_QUIT.toString(), Date.from(Instant.now())), MsgType.PLAYER_QUIT);
        }
    }

    @Listener(order = Order.LAST)
    @IsCancelled(Tristate.FALSE)
    public void onPlayerChat(MessageChannelEvent.Chat event, @First Player player) {
        //event.getMessage().toString()  full
        if (MCGuard.isSimulated(event)) {
            return;
        }
        plugin.countKarma(player, CountKarmaType.CHAT);

        GuardPlayer gPlayer = plugin.getGuardPlayer(player);
        if (gPlayer == null) {
            plugin.sendRabbit(new ActionPlayer(player.getName(), player.getUniqueId().toString(), event.getRawMessage().toPlain(), null, Date.from(Instant.now()), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
        } else if (gPlayer.isReport()) {
            event.setCancelled(true);
        } else {
            plugin.sendRabbit(new ActionPlayer(gPlayer, event.getRawMessage().toPlain(), null, Date.from(Instant.now()), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
            if (gPlayer.isConfirmation()) {
                event.setCancelled(true);
            }
        }
    }

    @Listener(order = Order.FIRST)
    @IsCancelled(Tristate.UNDEFINED)
    public void onPlayerChatAddReport(MessageChannelEvent.Chat event, @First Player player) {
        if (MCGuard.isSimulated(event)) {
            return;
        }
        plugin.countKarma(player, CountKarmaType.CHAT);
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp != null && gp.isReport()) {
            event.setCancelled(true);
            gp.getReport().addMsg(event.getRawMessage().toPlain());
            if (MsgType.getType(gp.getReport().getTypeMsg()) == MsgType.REPORT_ADD) {
                plugin.sendRabbit(gp.getReport(), MsgType.REPORT_ADD);
            } else if (MsgType.getType(gp.getReport().getTypeMsg()) == MsgType.REPORT_CREATE_ADD) {
                plugin.sendRabbit(gp.getReport(), MsgType.REPORT_CREATE_ADD);
            } else {
                gp.setReport(null);
            }
        }
    }

    @Listener(order = Order.LAST)
    @IsCancelled(Tristate.FALSE)
    public void onPlayerCommand(SendCommandEvent event, @First Player player) {
        if (MCGuard.isSimulated(event)) {
            return;
        }
        plugin.countKarma(player, CountKarmaType.CMD);
        GuardPlayer gPlayer = plugin.getGuardPlayer(player);
        if (gPlayer == null) {
            plugin.sendRabbit(new ActionPlayer(player.getName(), player.getUniqueId().toString(), event.getArguments(), event.getCommand(), Date.from(Instant.now()), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
        } else if (gPlayer.isConfirmation() && plugin.isModerationCmd(event.getCommand())) {
            plugin.sendRabbit(new ActionPlayer(gPlayer, event.getArguments(), event.getCommand(), Date.from(Instant.now()), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
            event.setCancelled(true);
        } else {
            plugin.sendRabbit(new ActionPlayer(gPlayer, false, event.getArguments(), event.getCommand(), Date.from(Instant.now()), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
        }
    }

    @Listener
    public void onPlayerInteract(InteractEvent event, @First Player player) {
        plugin.countKarma(player, CountKarmaType.INTERACT);
    }

    @Listener
    public void onBlockEvent(ChangeBlockEvent event, @First Player player) {
        plugin.countKarma(player, CountKarmaType.BLOCK);
    }

    @Listener
    public void onPlayerDeath(DestructEntityEvent.Death event, @Root DamageSource damageSource) {
        if (!(event.getTargetEntity() instanceof Player)) {
            return;
        }
        plugin.countKarma((Player) event.getTargetEntity(), CountKarmaType.DEATH);
    }

    @Listener
    public void onPlayerDispenseItem(DropItemEvent.Dispense event, @Root Player player) {
        plugin.countKarma(player, CountKarmaType.DROP);
    }

    @Listener
    public void onPlayerDispenseItem(DropItemEvent.Destruct event, @Root Player player) {
        plugin.countKarma(player, CountKarmaType.DROPDESTR);
    }

    @Listener
    public void onPlayerInteractEntity(InteractEntityEvent event, @First Player player) {
        plugin.countKarma(player, CountKarmaType.INTERACTENTITY);
    }

    @Listener
    public void onPlayerPickupItem(ChangeInventoryEvent event, @Root Player player) {
        plugin.countKarma(player, CountKarmaType.CHANGEINV);
    }

    @Listener(order = Order.FIRST)
    public void onPlayerDisconnect(ClientConnectionEvent.Disconnect event) {
        plugin.countKarma(event.getTargetEntity(), CountKarmaType.RESET);
    }

}
