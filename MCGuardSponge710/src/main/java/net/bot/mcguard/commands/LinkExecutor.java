package net.bot.mcguard.commands;

// @author ArtBorax
import java.net.MalformedURLException;
import java.net.URL;
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.ActionPlayer;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.rabbitmq.MsgType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.LiteralText.Builder;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;

public class LinkExecutor implements CommandExecutor {

    private final MCGuard plugin;

    public LinkExecutor(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("Только из игры."));
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0) {
            throw new CommandException(Text.of("Команда временно недоступна, попробуйте повторить попытку через 5 секунд."));
        }
        if (ctx.<Integer>getOne("kode").isPresent()) {
            if (ctx.<Integer>getOne("kode").get() > 999 && ctx.<Integer>getOne("kode").get() < 10000) {
                ActionPlayer ap = new ActionPlayer();
                ap.setPlayerId(gp.getPlayerId());
                ap.setType(MsgType.DISCORD_LINK.toString());
                ap.setMsg(Integer.toString(ctx.<Integer>getOne("kode").get()));
                plugin.sendRabbit(ap, MsgType.DISCORD_LINK);
                return CommandResult.success();
            }
        }
        try {
            Builder builder = Text.builder("");
            builder.append(Text.of(TextColors.RED, "Код должен состоять из 4 цифр, получить его можно в нашем дискорде по этой ссылке:\n"));
            builder.append(Text.of(TextColors.AQUA, Text.builder("http://mcborax.ru/discord").onClick(TextActions.openUrl(new URL("http://mcborax.ru/discord"))).build()));
            player.sendMessage(builder.build());
        } catch (MalformedURLException ex) {
        }
        return CommandResult.success();
    }
}
