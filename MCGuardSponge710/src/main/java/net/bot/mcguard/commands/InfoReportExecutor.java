package net.bot.mcguard.commands;

// @author ArtBorax
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.map.ReportMessage;
import net.bot.mcguard.rabbitmq.MsgType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class InfoReportExecutor implements CommandExecutor {

    private final MCGuard plugin;

    public InfoReportExecutor(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("Только из игры."));
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0) {
            throw new CommandException(Text.of("Команда временно недоступна, попробуйте повторить попытку через 5 секунд."));
        }
        if (!ctx.<Integer>getOne("number").isPresent()) {
            throw new CommandException(Text.of("Не верно указан номер репорта!"));
        }
        ReportMessage rm = new ReportMessage();
        rm.setPlayerId(gp.getPlayerId());
        rm.setTypeMsg(MsgType.REPORT_INFO.toString());
        rm.setReportId(ctx.<Integer>getOne("number").get());
        plugin.sendRabbit(rm, MsgType.REPORT_INFO);
        return CommandResult.success();
    }

}
