package net.bot.mcguard.commands;

// @author ArtBorax
import java.time.Instant;
import java.util.Date;
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.map.ReportMessage;
import net.bot.mcguard.rabbitmq.MsgType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class CompleteReportExecutor implements CommandExecutor {

    private final MCGuard plugin;

    public CompleteReportExecutor(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("Только из игры."));
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0 || !gp.isReport()) {
            throw new CommandException(Text.of("Сначала нужно создать репорт!"));
        }
        ReportMessage rm = gp.getReport();
        gp.setReport(null);
        if (MsgType.getType(rm.getTypeMsg()) == MsgType.REPORT_ADD) {
            rm.setTypeMsg(MsgType.REPORT_ADD_COMPL.toString());
            rm.setTime(Date.from(Instant.now()));
            plugin.sendRabbit(rm, MsgType.REPORT_ADD_COMPL);
            player.sendMessage(Text.of("§aДополнение отправлено в обработку."));
            return CommandResult.success();
        }
        if (MsgType.getType(rm.getTypeMsg()) == MsgType.REPORT_CREATE_ADD) {
            rm.setTypeMsg(MsgType.REPORT_CREATE_COMPL.toString());
            rm.setTime(Date.from(Instant.now()));
            plugin.sendRabbit(rm, MsgType.REPORT_CREATE_COMPL);
            player.sendMessage(Text.of("§aРепорт отправлен в обработку."));
            return CommandResult.success();
        }
        throw new CommandException(Text.of("Ошибка!"));

    }

}
