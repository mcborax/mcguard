package net.bot.mcguard.commands;

// @author ArtBorax
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.map.ReportMessage;
import net.bot.mcguard.rabbitmq.MsgType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;

public class CreateReportExecutor implements CommandExecutor {

    private final MCGuard plugin;

    public CreateReportExecutor(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext ctx) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(Text.of("Только из игры."));
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0) {
            throw new CommandException(Text.of("Команда временно недоступна, попробуйте повторить попытку через 5 секунд."));
        }
        if (gp.isReport()) {
            throw new CommandException(Text.of("Ты уже добавляешь описание для репорта: " + gp.getReport().getReportId() + "! Закончи этот процесс сначала!!!"));
        }
        ReportMessage rm = new ReportMessage();
        if (ctx.<User>getOne("player").isPresent()) {
            User user = ctx.<User>getOne("player").get();
            rm.setAccusedUsername(user.getName());
            rm.setAccusedUUID(user.getUniqueId().toString());
        } else if (ctx.<String>getOne("user").isPresent()) {
            rm.setAccusedUsername(ctx.<String>getOne("user").get());
        } else {
            throw new CommandException(Text.of("Укажи правильно ник игрока, на которого хочешь создать репорт."));
        }
        if (ctx.<Integer>getOne("type").isPresent()) {
            rm.setType(ctx.<Integer>getOne("type").get());
            if (ctx.<Integer>getOne("level").isPresent()) {
                rm.setLevel(ctx.<Integer>getOne("level").get());
                if (ctx.<Integer>getOne("text").isPresent()) {
                    rm.setMsg(ctx.<String>getOne("text").get());
                }
            }
        }
        rm.setPlayerId(gp.getPlayerId());
        rm.setTypeMsg(MsgType.REPORT_CREATE.toString());
        plugin.sendRabbit(rm, MsgType.REPORT_CREATE);
        return CommandResult.success();
    }

}
