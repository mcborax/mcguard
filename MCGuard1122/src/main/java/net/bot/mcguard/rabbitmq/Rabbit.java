package net.bot.mcguard.rabbitmq;

// @author ArtBorax
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import net.bot.mcguard.MCGuard;

public class Rabbit {

    private ConnectionFactory factory = new ConnectionFactory();
    private final ArrayBlockingQueue<Msg> queue;
    private Connection connection;
    private Channel channel;

    public Rabbit() {
        factory.setHost(MCGuard.getInstance().getConfig().getString("RabbitHost"));
        factory.setPort(MCGuard.getInstance().getConfig().getInt("RabbitPort"));
        factory.setUsername(MCGuard.getInstance().getConfig().getString("RabbitUser"));
        factory.setPassword(MCGuard.getInstance().getConfig().getString("RabbitPass"));
        factory.setAutomaticRecoveryEnabled(true);
        queue = new ArrayBlockingQueue(1000000, true);
        int serverId = MCGuard.getInstance().getConfig().getInt("ServerID");
        for (MsgType type : MsgType.values()) {
            type.setServerId(serverId);
        }
        treading();
    }

    private void connect() {
        try {
            //MCGuard.getInstance().getLogger().info("§eConnection RabbitMQ....");
            if (connection == null || !connection.isOpen()) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (Exception ex) {
                    }
                }
                connection = factory.newConnection();
            }
            if (channel == null || !channel.isOpen()) {
                if (channel != null) {
                    try {
                        channel.close();
                    } catch (Exception ex) {
                    }
                }
                channel = connection.createChannel();
            }
            channel.queueDeclare(MCGuard.getInstance().getConfig().getString("QueueNameC"), true, false, false, null);
            channel.queueDeclare(MCGuard.getInstance().getConfig().getString("QueueNameS"), true, false, false, null);
            MCGuard.getInstance().rabbitListenerQueue();
            MCGuard.getInstance().getLogger().info("§aConnect ready RabbitMQ!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void closeConnect() {
        if (channel != null) {
            try {
                channel.close();
                MCGuard.getInstance().getLogger().info("§eChannel RabbitMQ close.");
            } catch (Exception ex) {
            }
        }
        if (connection != null) {
            try {
                connection.close();
                MCGuard.getInstance().getLogger().info("§eConnection RabbitMQ close.");
            } catch (Exception ex) {
            }
        }
    }

    public void putMsg(Msg msg) {
        while (!queue.offer(msg)) {
            queue.remove();
            MCGuard.getInstance().getLogger().warning("§cQueue OVERLOAD!");
        }
    }

    private void treading() {
        Thread t = new Thread() {
            @Override
            public void run() {
                MCGuard.getInstance().getLogger().info("§aThread RabbitMQ start.");
                while (true) {
                    try {
                        Msg msg = queue.take();
                        while (!send(msg)) {
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException ex) {
                        break;
                    }
                }
                closeConnect();
                MCGuard.getInstance().getLogger().info("§eThread RabbitMQ stop.");
            }
        };
        t.start();
    }

    private boolean send(Msg msg) {
        if (connection != null && connection.isOpen() && channel != null && channel.isOpen()) {
            try {
                channel.basicPublish(msg.getExchangeName(), msg.getQueueName(), msg.getProps(), msg.getMsg());
                return true;
            } catch (IOException ex) {
            }
        } else {
            connect();
        }
        return false;
    }

    public void queueDeclare(String queueName, final IRabbitReceiving receive) throws IOException {
        if (connection == null || !connection.isOpen() || channel == null || !channel.isOpen()) {
            connect();
        }
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body) throws IOException {
                if (properties.getAppId() == null || properties.getAppId().isEmpty()) {
                    channel.basicAck(envelope.getDeliveryTag(), false);
                    return;
                }
                if (MCGuard.getInstance().getConfig().getString("ServerID").equals(properties.getAppId())) {
                    if (receive.receive(properties.getMessageId(), new String(body, "UTF-8"))) {
                        channel.basicAck(envelope.getDeliveryTag(), false);
                    }
                } else {
                    channel.basicNack(envelope.getDeliveryTag(), false, true);
                }
            }
        };
        channel.basicConsume(queueName, consumer);
        MCGuard.getInstance().getLogger().info("RabbitMQ channel [" + queueName + "] declared.");
    }

}
