package net.bot.mcguard.commands;

// @author ArtBorax
import java.util.Date;
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.ActionPlayer;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.map.ReportMessage;
import net.bot.mcguard.rabbitmq.MsgType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReportCmd implements CommandExecutor {

    private final MCGuard plugin;

    public ReportCmd(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender src, Command cmnd, String string, String[] args) {
        if (src == null) {
            return false;
        }
        if (!(src instanceof Player)) {
            src.sendMessage("Только из игры.");
            return false;
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0) {
            player.sendMessage(ChatColor.RED + "Команда временно недоступна, попробуйте повторить попытку через 5 секунд.");
            return false;
        }
        if (args.length == 0) {
            ActionPlayer ap = new ActionPlayer();
            ap.setPlayerId(gp.getPlayerId());
            ap.setType(MsgType.GET_MENU_MAIN.toString());
            plugin.sendRabbit(ap, MsgType.GET_MENU_MAIN);
        } else if (args[0].equalsIgnoreCase("cancel")) {
            if (!gp.isReport()) {
                player.sendMessage(ChatColor.RED + "Отменять уже нечего!");
            }
            gp.setReport(null);
            player.sendMessage("§aРепорт отменен.");
        } else if (args[0].equalsIgnoreCase("stat")) {
            ActionPlayer ap = new ActionPlayer();
            ap.setPlayerId(gp.getPlayerId());
            ap.setType(MsgType.GET_PLAYER_STAT.toString());
            if (args.length == 1) {
                ap.setUsername(player.getName());
            } else if (args.length > 1) {
                if (args[1].length() < 3) {
                    player.sendMessage(ChatColor.RED + "Укажи правильно ник игрока!");
                    return true;
                }
                ap.setUsername(args[1]);
            }
            plugin.sendRabbit(ap, MsgType.GET_PLAYER_STAT);
        } else if (args[0].equalsIgnoreCase("log")) {
            ActionPlayer ap = new ActionPlayer();
            ap.setPlayerId(gp.getPlayerId());
            ap.setType(MsgType.GET_LOG_CHAT.toString());

            if (args.length == 1) {
                plugin.sendRabbit(ap, MsgType.GET_LOG_CHAT);
            }
            if (args.length == 2) {
                try {
                    ap.setMsg(Integer.toString(Integer.parseInt(args[1])));
                } catch (Exception ex) {
                    Player target = Bukkit.getServer().getPlayer(args[1]);
                    if (target != null && target.getName().equalsIgnoreCase(args[1])) {
                        ap.setUsername(target.getName());
                    } else {
                        ap.setUsername(args[1]);
                    }
                }
                plugin.sendRabbit(ap, MsgType.GET_LOG_CHAT);
            }
            if (args.length >= 3) {
                Player target = Bukkit.getServer().getPlayer(args[1]);
                if (target != null && target.getName().equalsIgnoreCase(args[1])) {
                    ap.setUsername(target.getName());
                } else {
                    ap.setUsername(args[1]);
                }
                try {
                    ap.setMsg(Integer.toString(Integer.parseInt(args[2])));
                    plugin.sendRabbit(ap, MsgType.GET_LOG_CHAT);
                } catch (Exception ex) {
                    player.sendMessage(ChatColor.RED + "Укажи правильно номер страницы!");
                }
            }
            return true;
        } else if (args[0].equalsIgnoreCase("new")) {
            if (gp.isReport()) {
                player.sendMessage(ChatColor.RED + "Ты уже добавляешь описание для репорта: " + gp.getReport().getReportId() + "! Закончи этот процесс сначала!!!");
                return false;
            }
            ReportMessage rm = new ReportMessage();
            rm.setPlayerId(gp.getPlayerId());

            rm.setTypeMsg(MsgType.REPORT_CREATE.toString());
            if (args.length > 1) {
                Player target = Bukkit.getServer().getPlayer(args[1]);
                if (target != null && target.getName().equalsIgnoreCase(args[1])) {
                    rm.setAccusedUsername(target.getName());
                } else {
                    rm.setAccusedUsername(args[1]);
                }
            } else {
                player.sendMessage(ChatColor.RED + "Укажи ник игрока!");
                return true;
            }

            int type = -2;
            if (args.length > 2) {
                try {
                    type = Integer.parseInt(args[2]);
                } catch (Exception ex) {
                }
            }
            int level = -3;
            if (args.length > 3) {
                try {
                    level = Integer.parseInt(args[3]);
                } catch (Exception ex) {
                }
            }
            StringBuilder sbArgs = new StringBuilder();
            if (args.length > 4) {
                for (int i = 4; i < args.length; i++) {
                    sbArgs.append(args[i]).append(" ");
                }
            }
            if (type != -2) {
                rm.setType(type);
                if (level != -3) {
                    rm.setLevel(level);
                    if (sbArgs.length() > 0) {
                        rm.setMsg(sbArgs.toString().trim());
                    }
                }
            }
            plugin.sendRabbit(rm, MsgType.REPORT_CREATE);
        } else if (args[0].equalsIgnoreCase("add")) {
            if (gp.isReport()) {
                player.sendMessage(ChatColor.RED + "Ты уже добавляешь описание для репорта: " + gp.getReport().getReportId() + "! Закончи этот процесс сначала!!!");
                return false;
            }
            int number = 0;
            if (args.length > 1) {
                try {
                    number = Integer.parseInt(args[1]);
                } catch (Exception ex) {
                }
            }
            if (number == 0) {
                player.sendMessage(ChatColor.RED + "Не верно указан номер репорта!");
                return false;
            }
            ReportMessage rm = new ReportMessage();
            rm.setPlayerId(gp.getPlayerId());
            rm.setTypeMsg(MsgType.REPORT_ADD.toString());
            rm.setReportId(number);
            plugin.sendRabbit(rm, MsgType.REPORT_ADD);
        } else if (args[0].equalsIgnoreCase("complete")) {
            if (!gp.isReport()) {
                player.sendMessage(ChatColor.RED + "Сначала нужно создать репорт!");
            }
            ReportMessage rm = gp.getReport();
            gp.setReport(null);
            if (MsgType.getType(rm.getTypeMsg()) == MsgType.REPORT_ADD) {
                rm.setTypeMsg(MsgType.REPORT_ADD_COMPL.toString());
                rm.setTime(new Date());
                plugin.sendRabbit(rm, MsgType.REPORT_ADD_COMPL);
                player.sendMessage("§aДополнение отправлено в обработку.");
            } else if (MsgType.getType(rm.getTypeMsg()) == MsgType.REPORT_CREATE_ADD) {
                rm.setTypeMsg(MsgType.REPORT_CREATE_COMPL.toString());
                rm.setTime(new Date());
                plugin.sendRabbit(rm, MsgType.REPORT_CREATE_COMPL);
                player.sendMessage("§aРепорт отправлен в обработку.");
            } else {
                player.sendMessage(ChatColor.RED + "Ошибка!");
                return false;
            }
        } else if (args[0].equalsIgnoreCase("consider")) {
            if (gp.isReport()) {
                player.sendMessage(ChatColor.RED + "Ты уже добавляешь описание для репорта: " + gp.getReport().getReportId() + "! Закончи этот процесс сначала!!!");
                return false;
            }
            int number = 0;
            if (args.length > 1) {
                try {
                    number = Integer.parseInt(args[1]);
                } catch (Exception ex) {
                }
            }
            if (number == 0) {
                player.sendMessage(ChatColor.RED + "Не верно указан номер репорта!");
                return false;
            }
            int level = -3;
            if (args.length > 2) {
                try {
                    level = Integer.parseInt(args[2]);
                } catch (Exception ex) {
                }
            }
            StringBuilder sbArgs = new StringBuilder();
            if (args.length > 3) {
                for (int i = 3; i < args.length; i++) {
                    sbArgs.append(args[i]).append(" ");
                }
            }
            ReportMessage rm = new ReportMessage();
            rm.setLevel(-2);
            if (level != -3) {
                rm.setLevel(level);
                if (sbArgs.length() > 0) {
                    rm.setMsg(sbArgs.toString().trim());
                    rm.setTime(new Date());
                }
            }
            rm.setPlayerId(gp.getPlayerId());
            rm.setTypeMsg(MsgType.REPORT_CONS.toString());
            rm.setReportId(number);
            plugin.sendRabbit(rm, MsgType.REPORT_CONS);
        } else if (args[0].equalsIgnoreCase("info")) {
            int number = 0;
            if (args.length > 1) {
                try {
                    number = Integer.parseInt(args[1]);
                } catch (Exception ex) {
                }
            }
            if (number == 0) {
                player.sendMessage(ChatColor.RED + "Не верно указан номер репорта!");
                return false;
            }
            ReportMessage rm = new ReportMessage();
            rm.setPlayerId(gp.getPlayerId());
            rm.setTypeMsg(MsgType.REPORT_INFO.toString());
            rm.setReportId(number);
            plugin.sendRabbit(rm, MsgType.REPORT_INFO);
        } else if (args[0].equalsIgnoreCase("top")) {
            ActionPlayer ap = new ActionPlayer();
            ap.setPlayerId(gp.getPlayerId());
            ap.setType(MsgType.GET_TOP_KARMA.toString());
            plugin.sendRabbit(ap, MsgType.GET_TOP_KARMA);
        } else {
            player.sendMessage(ChatColor.RED + "Подсказка: /report [log/new/add/info/consider/complete/cancel/stat/top]");
        }
        return true;
    }
}
