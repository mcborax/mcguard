package net.bot.mcguard.commands;

// @author ArtBorax
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.ActionPlayer;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.rabbitmq.MsgType;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LinkCmd implements CommandExecutor {

    private final MCGuard plugin;

    public LinkCmd(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender src, Command cmnd, String string, String[] args) {
        if (src == null) {
            return false;
        }
        if (!(src instanceof Player)) {
            src.sendMessage("Только из игры.");
            return false;
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0) {
            player.sendMessage(ChatColor.RED + "Команда временно недоступна, попробуйте повторить попытку через 5 секунд.");
            return false;
        }
        ActionPlayer ap = new ActionPlayer();
        ap.setPlayerId(gp.getPlayerId());
        ap.setType(MsgType.DISCORD_LINK.toString());
        if (args.length > 0) {
            try {
                int kode = Integer.parseInt(args[0]);
                if (kode > 999 && kode < 10000) {
                    ap.setMsg(Integer.toString(kode));
                    plugin.sendRabbit(ap, MsgType.DISCORD_LINK);
                    return true;
                }
            } catch (Exception ex) {
            }
        } else {
            player.sendMessage(ChatColor.RED + "/link " + ChatColor.AQUA + "<КОД_КОТОРЫЙ_ТЫ_ПОЛУЧИЛ_В_ДИСКОРДЕ>");
            return true;
        }
        player.sendMessage(ChatColor.RED + "Код должен состоять из 4 цифр, получить его можно в нашем дискорде по этой ссылке:");
        player.sendMessage("http://mcborax.ru/discord");
        return true;
    }
}
