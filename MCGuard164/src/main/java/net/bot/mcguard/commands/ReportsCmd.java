package net.bot.mcguard.commands;

// @author ArtBorax
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.ActionPlayer;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.rabbitmq.MsgType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReportsCmd implements CommandExecutor {

    private final MCGuard plugin;

    public ReportsCmd(MCGuard plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender src, Command cmnd, String string, String[] args) {
        if (src == null) {
            return false;
        }
        if (!(src instanceof Player)) {
            src.sendMessage("Только из игры.");
            return false;
        }
        Player player = (Player) src;
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp == null || gp.getPlayerId() == 0) {
            player.sendMessage(ChatColor.RED + "Команда временно недоступна, попробуйте повторить попытку через 5 секунд.");
            return false;
        }
        if (gp.isReport()) {
            player.sendMessage(ChatColor.RED + "Ты уже добавляешь описание для репорта: " + gp.getReport().getReportId() + "! Закончи этот процесс сначала!!!");
            return false;
        }
        ActionPlayer ap = new ActionPlayer();
        ap.setPlayerId(gp.getPlayerId());
        ap.setType(MsgType.GET_REPORTS.toString());

        if (args.length == 0) {
            plugin.sendRabbit(ap, MsgType.GET_REPORTS);
        }
        if (args.length == 1) {
            try {
                ap.setMsg(Integer.toString(Integer.parseInt(args[0])));
            } catch (Exception ex) {
                Player target = Bukkit.getServer().getPlayer(args[0]);
                if (target != null && target.getName().equalsIgnoreCase(args[0])) {
                    ap.setUsername(target.getName());
                } else {
                    ap.setUsername(args[0]);
                }
            }
            plugin.sendRabbit(ap, MsgType.GET_REPORTS);
        }
        if (args.length >= 2) {
            Player target = Bukkit.getServer().getPlayer(args[0]);
            if (target != null && target.getName().equalsIgnoreCase(args[0])) {
                ap.setUsername(target.getName());
            } else {
                ap.setUsername(args[0]);
            }
            try {
                ap.setMsg(Integer.toString(Integer.parseInt(args[1])));
                plugin.sendRabbit(ap, MsgType.GET_REPORTS);
            } catch (Exception ex) {
                player.sendMessage(ChatColor.RED + "Укажи правильно номер страницы!");
            }
        }
        return true;
    }
}
