package net.bot.mcguard.listeners;

// @author ArtBorax
import java.util.Date;
import net.bot.mcguard.CountKarmaType;
import net.bot.mcguard.MCGuard;
import net.bot.mcguard.map.ActionPlayer;
import net.bot.mcguard.map.GuardPlayer;
import net.bot.mcguard.rabbitmq.MsgType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;

//    @EventHandler(priority = EventPriority.LOWEST) 1
//    @EventHandler(priority = EventPriority.LOW) 2
//    @EventHandler 3
//    @EventHandler(priority = EventPriority.NORMAL) 4
//    @EventHandler(priority = EventPriority.HIGH) 5
//    @EventHandler(priority = EventPriority.HIGHEST) 6
//    @EventHandler(priority = EventPriority.MONITOR) 7
//    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true) если отменен то не сработает
//    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = false) сработает в любом случае
public class PlayerEventHandler implements Listener {

    private final MCGuard plugin;

    public PlayerEventHandler(MCGuard plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (player == null) {
            return;
        }
        plugin.countKarma(player, CountKarmaType.RESET);
        GuardPlayer gPlayer = plugin.getGuardPlayer(player);
        if (gPlayer != null && gPlayer.getPlayerId() != 0) {
            plugin.sendRabbit(new ActionPlayer(gPlayer.getPlayerId(), MsgType.PLAYER_JOIN.toString(), new Date()), MsgType.PLAYER_JOIN);
        } else {
            plugin.sendRabbit(new ActionPlayer(player.getName(), null, MsgType.PLAYER_JOIN.toString(), new Date()), MsgType.PLAYER_JOIN);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (player == null) {
            return;
        }
        plugin.countKarma(player, CountKarmaType.RESET);
        GuardPlayer gPlayer = plugin.getGuardPlayer(player);
        if (gPlayer != null && gPlayer.getPlayerId() != 0) {
            plugin.sendRabbit(new ActionPlayer(gPlayer.getPlayerId(), MsgType.PLAYER_QUIT.toString(), new Date()), MsgType.PLAYER_QUIT);
            plugin.removeCachPlayer(player.getName());
        } else {
            plugin.sendRabbit(new ActionPlayer(player.getName(), null, MsgType.PLAYER_QUIT.toString(), new Date()), MsgType.PLAYER_QUIT);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if (player == null || event.isCancelled()) {
            return;
        }
        plugin.countKarma(player, CountKarmaType.CHAT);

        GuardPlayer gPlayer = plugin.getGuardPlayer(player);
        if (gPlayer != null && gPlayer.isConfirmation() && !event.isAsynchronous()) {
            return;
        }

        if (gPlayer == null) {
            plugin.sendRabbit(new ActionPlayer(player.getName(), null, event.getMessage(), null, new Date(), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
        } else if (gPlayer.isReport()) {
            event.setCancelled(true);
        } else {
            plugin.sendRabbit(new ActionPlayer(gPlayer, event.getMessage(), null, new Date(), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
            if (gPlayer.isConfirmation()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onPlayerChatAddReport(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if (player == null) {
            return;
        }
        plugin.countKarma(player, CountKarmaType.CHAT);
        GuardPlayer gp = plugin.getGuardPlayer(player);
        if (gp != null && gp.isReport()) {
            event.setCancelled(true);
            gp.getReport().addMsg(event.getMessage());
            if (MsgType.getType(gp.getReport().getTypeMsg()) == MsgType.REPORT_ADD) {
                plugin.sendRabbit(gp.getReport(), MsgType.REPORT_ADD);
            } else if (MsgType.getType(gp.getReport().getTypeMsg()) == MsgType.REPORT_CREATE_ADD) {
                plugin.sendRabbit(gp.getReport(), MsgType.REPORT_CREATE_ADD);
            } else {
                gp.setReport(null);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        if (player == null || event.isCancelled()) {
            return;
        }
        plugin.countKarma(player, CountKarmaType.CMD);

        String[] arrayOfString = event.getMessage().split(" ");
        if (arrayOfString.length == 0 || arrayOfString[0].length() < 2) {
            return;
        }

        String cmd = arrayOfString[0].toLowerCase().substring(1);
        StringBuilder sbArgs = new StringBuilder();
        if (arrayOfString.length > 1) {
            for (int i = 1; i < arrayOfString.length; i++) {
                sbArgs.append(arrayOfString[i]).append(" ");
            }
        }
        GuardPlayer gPlayer = plugin.getGuardPlayer(player);
        if (gPlayer == null) {
            plugin.sendRabbit(new ActionPlayer(player.getName(), null, sbArgs.toString(), cmd, new Date(), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
        } else if (gPlayer.isConfirmation() && plugin.isModerationCmd(cmd)) {
            plugin.sendRabbit(new ActionPlayer(gPlayer, sbArgs.toString(), cmd, new Date(), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
            event.setCancelled(true);
        } else {
            plugin.sendRabbit(new ActionPlayer(gPlayer, false, sbArgs.toString(), cmd, new Date(), MsgType.CHAT_MSG.toString()), MsgType.CHAT_MSG);
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event == null || event.getPlayer() == null) {
            return;
        }
        if (event.getClickedBlock() == null) {
            plugin.countKarma(event.getPlayer(), CountKarmaType.INTERACT);
        } else {
            plugin.countKarma(event.getPlayer(), CountKarmaType.BLOCK);
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        plugin.countKarma((Player) event.getEntity(), CountKarmaType.DEATH);
    }

    @EventHandler
    public void onPlayerDispenseItem(PlayerDropItemEvent event) {
        if (event.getPlayer() == null) {
            return;
        }
        plugin.countKarma(event.getPlayer(), CountKarmaType.DROP);
    }

//    @Listener
//    public void onPlayerDispenseItem(DropItemEvent.Destruct event, @Root Player player) {
//        plugin.countKarma(player, CountKarmaType.DROPDESTR);
//    }
    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (event.getPlayer() == null) {
            return;
        }
        plugin.countKarma(event.getPlayer(), CountKarmaType.INTERACTENTITY);
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (event.getPlayer() == null) {
            return;
        }
        plugin.countKarma(event.getPlayer(), CountKarmaType.CHANGEINV);
    }

}
